#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define A 5
#define B 5
int main ()
{
	int matrix [A] [B];
	int i,j,posnum=0,negnum=0;
	srand(time (0));
	for (i=0; i<A; i++)
		for (j=0; j<B; j++)
			matrix [i][j]=rand()%15-5;
	for (i=0; i<A; i++)
	{
		for (j=0; j<B; j++)
			printf ("%2d ", matrix[i][j]);
		putchar ('\n');
	}
	for (i=0; i<A; i++)
		for (j=0; j<B; j++)
			if (matrix [i][j]>=0)
				posnum++;
			else
				negnum++;
	printf ("positive=%d, negative=%d\n", posnum, negnum);
	if (posnum>negnum)
		printf ("positive num biggest");
	else
		printf ("negative num biggest");
	putchar ('\n');
	return 0;
}
