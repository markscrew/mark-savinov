#include "stdafx.h"
#include <iostream>
#include <time.h>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int max_len = 0;
	int number, num;
	int j;
	int k = 0;
	int time;

	cin >> num;
	for(int i = 2; i <= num; i++){
		cout << "Processing...\n";
		time = clock();
		cout << "Time " << clock() << '\n';
		cout << "Number " << i;
		j = i;
		do{
			if(j % 2 == 0)
				j = j/2;
			else
				j = (3*j+1)/2;
			k++;
		}while(j != 1);
		if(k > max_len){
			max_len = k;
			number = i;
		}
		k = 0;
		system("CLS");
	}

	cout << "number " << number << '\n';
	cout << "max_len " << max_len << '\n';
	cout << "time " << time << '\n';

	return 0;
}