#include <iostream> 
#include <cstdlib> 
#include <ctime> 
 
int main() 
{   
    const int size = 200;
    srand((unsigned) time(0)); 
    int i, sum = 0;
    int array[size];  
    int max_index, min_index;
 
    std::cout << "Primary array " << std::endl;
    for ( i = 0; i < size; ++i)  
    {
        array[i] = rand() % 201 - 100; 
        std::cout << array[i] << " "; 
    } 
    std::cout << std::endl;
 
    for ( i = 0; i < size; ++i) 
        if(array[i] > 0) 
        { 
            max_index = i; 
            break; 
        } 
 
        for ( i = size - 1; i >= 0; --i) 
            if(array[i] < 0) 
            { 
                min_index = i; 
            } 
 
            for ( i = max_index+1; i < min_index; ++i) 
                sum += array[i]; 
            std::cout << "Sum is " << sum << std::endl;
}