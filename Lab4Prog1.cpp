#include <iostream>
#include <string.h>
#include <cstdlib>
#include <ctime>


using namespace std;

int wordsCount = 0;

int main(){
	srand(time(NULL));
	char string[80];
	//считываем строку
	gets(string);
	//в wordsCound записываем количество слов в строке
	for (int i = 0; i <= strlen(string); i++)
		if(string[i] == ' ' || string[i] == '\0') 
			wordsCount++;
	//выделяем память под массив указателей на первые буквы
	char **strMas = new char*[wordsCount];
	for (int i = 0; i < wordsCount; i++)
		strMas[i] = new char[20];
	//объявляем счетчики
	int j = 0, g = 0;
	
	for (int i = 0; i <= strlen(string); i++)
		if(string[i] != ' ' && string[i] != '\0'){
			strMas[j][g] = string[i];
			g++;
		}
		else{
			strMas[j][g] = '\0';
			g = 0;
			j++;
		}
		//объявляем массив, в котором слова будут перемешиваться
		char *temp = new char[20];
		//слова перемешиваются
		for (int i = 0; i < 50; i++){
			int t = rand() % wordsCount;
			int tt = (rand() * rand()) % wordsCount;
			temp = strMas[t];
			strMas[t] = strMas[tt];
			strMas[tt] = temp;
		}
		//и выводятся
		for (int i = 0; i < wordsCount; i++){
			if(strMas[i][0]=='\n') break;
			printf("%s ",strMas[i]);
		}

	return 0;
}