#include <iostream>
#include <string.h>
 
using namespace std;

int i = 0;

int new_atoi(const char* string){
	if(!string) return 0;
	int result = 0;
 
	//пропускаем пробельные символы
	while(*string == ' ' || *string == '\t' ) string++;
 
	// получаем знак числа
	bool bNegativeSign = (*string == '-');
	
	// переходим на следующий символ после знака
	if(bNegativeSign || *string=='+') string++;
	
	while(*string) {
		if( (*string < '0' || *string> '9') ) break;
		result = result * 10 + (*string - '0');
		string++;
	}
	return ( bNegativeSign ? -result : result );
}
 
void main(){
	cout << new_atoi("12345") << '\n';
}